const mongoose = require('mongoose');

const ShortUrlSchema = new mongoose.Schema({
    originalUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        required: true
    }
});

const ShortUrl = mongoose.model('ShortUrl', ShortUrlSchema);
module.exports = ShortUrl;