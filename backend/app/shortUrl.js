const express = require('express');
const ShortUrl = require('../models/ShortUrl');
const {nanoid} = require('nanoid');
const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const data = req.body;

        data.shortUrl = nanoid(6);

        const shortUrl = new ShortUrl(data);
        await shortUrl.save();

        res.send(shortUrl);
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    }
});

router.get('/:shortUrl', async (req, res) => {
    try {
        const url = await ShortUrl.findOne({shortUrl: req.params.shortUrl});
        if (url) {
            res.status(301).redirect(url.originalUrl);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});



module.exports = router;