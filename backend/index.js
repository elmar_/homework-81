const express = require('express');
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');
const cors = require('cors');
const shortUrl = require('./app/shortUrl');

const app = express();
app.use(cors());
app.use(express.json());

app.use('/shortUrl', shortUrl);

const port = '8000';

const run = async () => {

    await mongoose.connect('mongodb://localhost/shortenUrl', {useNewUrlParser: true, useUnifiedTopology: true});

    app.listen(port, () => {
        console.log('Port ' + port);
    });
};

run().catch(console.error);

