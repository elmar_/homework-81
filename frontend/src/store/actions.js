import axiosLink from "../axiosLink";

export const SHORT_URL_REQUEST = 'SHORT_URL_REQUEST';
export const SHORT_URL_SUCCESS = 'SHORT_URL_SUCCESS';
export const SHORT_URL_ERROR = 'SHORT_URL_ERROR';

export const shortUrlRequest = () => ({type: SHORT_URL_REQUEST});
export const shortUrlSuccess = data => ({type: SHORT_URL_SUCCESS, data});
export const shortUrlError = () => ({type: SHORT_URL_ERROR});

export const shortUrl = originalUrl => {
    return async dispatch => {
        try {
            dispatch(shortUrlRequest());
            const response = await axiosLink.post('/shortUrl', originalUrl);
            dispatch(shortUrlSuccess(response.data));
        } catch (e) {
            dispatch(shortUrlError());
        }
    }
}
