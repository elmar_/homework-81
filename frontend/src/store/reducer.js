import {SHORT_URL_ERROR, SHORT_URL_REQUEST, SHORT_URL_SUCCESS} from "./actions";

const initialState = {
    shortUrl: '',
    loading: false,
    error: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SHORT_URL_REQUEST:
            return {...state, loading: true};
        case SHORT_URL_SUCCESS:
            return {...state, shortUrl: action.data.shortUrl, error: false, loading: false};
        case SHORT_URL_ERROR:
            return {...state, loading: false, error: true};
        default:
            return state;
    }
};

export default reducer;