import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {shortUrl} from "./store/actions";
import './App.css';

const App = () => {
    const [state, setState] = useState({
        originalUrl: ''
    });
    const dispatch = useDispatch();
    const loading = useSelector(state => state.loading);
    const error = useSelector(state => state.error);
    const newUrl = useSelector(state => state.shortUrl);

    const http = 'http://localhost:8000/shortUrl/';


    const inputHandler = e => {
        const {name, value} = e.target;

        setState({
            [name]: value
        });
    };

    const shortenUrl = () => {
        dispatch(shortUrl(state));
    };

    let alert = null;
    if (error) {
        alert = <p>Problem in fetching</p>;
    }

    let url = null;
    if (newUrl) {
        const shortUrl = http + newUrl;
        url = (
            <div>
                <p>Your link now looks like this</p>
                <a href={shortUrl}>{shortUrl}</a>
            </div>
        );
    }

    return (
        <div>
            <div className="container">
                <h2 className="title">Shorten your link</h2>
                <input type="text" className="input" name="originalUrl" onChange={inputHandler} />
                <button type="button" className="btn" onClick={shortenUrl} disabled={loading}>Shorten</button>
                {alert}
                {url}
            </div>
        </div>
    );
};

export default App;